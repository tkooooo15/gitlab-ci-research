import os


def get_path():
    return os.path.dirname(os.path.abspath(__file__))


def add_func(a, b):
    return a + b


if __name__ == "__main__":
    print(get_path())
