FROM python:3.8.16-slim-buster

ENV TZ=Asia/Tokyo

# 初期化
RUN apt-get update -y && apt-get upgrade -y
# Git
RUN apt-get install -y git
# pip
RUN python3 -m pip install --upgrade pip 


# Linter
RUN pip install black flake8 pytk

# pytest
RUN pip install pytest pytest-cov